# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased]

## [v1.1.0]

### Deprecated

- Deprecate a constructor of `StarDictInfo` class. It is recommended not to create `StarDictInfo` object directly,
  and create it with `StarDictLoader#load` through `StarDictDictionary.loadDictionary`.
- When you want to create the `StarDictInfo` object directly, you can use `StarDictInfo.create` function.

### Changed

- Bump Gradle@8.6

[Unreleased]: https://codeberg.org/miurahr/stardict4j/compare/v1.1.0...HEAD
[v1.1.0]: https://codeberg.org/miurahr/stardict4j/compare/v1.0.0...v1.1.0
