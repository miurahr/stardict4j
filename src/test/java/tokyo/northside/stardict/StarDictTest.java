/*
 * Stardict4j - access library for stardict format.
 * Copyright (C) 2022 Hiroshi Miura.
 * Copyright (C) 2010 Alex Buloichik
 *               2015 Hiroshi Miura, Aaron Madlon-Kay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package tokyo.northside.stardict;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/**
 * Dictionary test.
 *
 * @author Alex Buloichik
 * @author Hiroshi Miura
 * @author Aaron Madlon-Kay
 */
public class StarDictTest {

    @Test
    public void testReadFileDict() throws Exception {
        try (StarDictDictionary dict =
                StarDictDictionary.loadDictionary(new File("src/test/resources/dicts/latin-francais.ifo"))) {
            assertEquals("latin-français", dict.getDictionaryName());
            assertEquals("2.4.2", dict.getDictionaryVersion());
            StarDictInfo info = dict.getInformation();
            assertEquals("m", info.getSametypesequence());
            assertEquals("Yves Ouvrard", info.getAuthor());
            assertEquals("http://www.collatinus.org/", info.getWebsite());
            assertTrue(info.getDescription().startsWith("Dictionnaire"));
            assertEquals("1er Novembre 2007", info.getDate());
            assertEquals(10451, info.getWordCount());
            assertEquals(10451, dict.data.size());

            String word = "testudo";
            List<Map.Entry<String, IndexEntry>> data = dict.data.lookUp(word);
            assertEquals(1, data.size());

            List<StarDictDictionary.Entry> result = dict.readArticles(word);
            assertEquals(1, result.size());
            assertEquals(word, result.get(0).getWord());
            assertEquals("dinis, f. : tortue", result.get(0).getArticle());

            // Test prediction
            word = "testu";
            result = dict.readArticles(word);
            assertTrue(result.isEmpty());
            result = dict.readArticlesPredictive(word);
            assertEquals(1, result.size());
            assertEquals("testudo", result.get(0).getWord());
            assertEquals("dinis, f. : tortue", result.get(0).getArticle());
            assertEquals(StarDictDictionary.EntryType.MEAN, result.get(0).getType());
        }
    }

    @Test
    public void testReadZipDict() throws Exception {
        try (StarDictDictionary dict =
                StarDictDictionary.loadDictionary(new File("src/test/resources/dicts-zipped/latin-francais.ifo"))) {
            assertEquals("latin-français", dict.getDictionaryName());
            assertEquals("2.4.2", dict.getDictionaryVersion());
            assertEquals(10451, dict.data.size());

            String word = "testudo";
            List<Map.Entry<String, IndexEntry>> data = dict.data.lookUp(word);
            assertEquals(1, data.size());
            List<StarDictDictionary.Entry> result = dict.readArticles(word);
            assertEquals(1, result.size());
            assertFalse(result.isEmpty());
            assertEquals(word, result.get(0).getWord());
            assertEquals("dinis, f. : tortue", result.get(0).getArticle());
            assertEquals(StarDictDictionary.EntryType.MEAN, result.get(0).getType());
        }
    }

    @Test
    public void testReadNoSameTypeDict() throws Exception {
        try (StarDictDictionary dict = StarDictDictionary.loadDictionary(
                new File("src/test/resources/dicts-nosametype/freedict-eng-jpn.ifo"), 500)) {
            assertEquals("freedict-eng-jpn.index", dict.getDictionaryName());
            assertEquals("3.0.0", dict.getDictionaryVersion());
            assertEquals(26989, dict.data.size());
            String word = "test";
            List<Map.Entry<String, IndexEntry>> data = dict.data.lookUp(word);
            assertEquals(1, data.size());
            List<StarDictDictionary.Entry> result = dict.readArticles(word);
            assertEquals(1, result.size());
            assertNull(result.get(0).getType());
            assertTrue(result.get(0).getArticle().startsWith("h")); // html type
            assertEquals("test //test", result.get(0).getArticle().substring(1, 12));
        }
    }

    @Test
    public void testReadDictPango() throws Exception {
        try (StarDictDictionary dict =
                StarDictDictionary.loadDictionary(new File("src/test/resources/dicts-pango/english-czech.ifo"))) {
            assertEquals("GNU/FDL Anglicko-Český slovník", dict.getDictionaryName());
            assertEquals("2.4.2", dict.getDictionaryVersion());
            StarDictInfo info = dict.getInformation();
            assertEquals("g", info.getSametypesequence());
            assertEquals("Stardicter", info.getAuthor());
            assertEquals("https://cihar.com/software/slovnik/", info.getWebsite());
            assertEquals("2021.04.01", info.getDate());
            assertEquals(88611, info.getWordCount());

            String word = "lookup";
            List<Map.Entry<String, IndexEntry>> data = dict.data.lookUp(word);
            assertEquals(1, data.size());

            List<StarDictDictionary.Entry> result = dict.readArticles(word);
            assertEquals(1, result.size());
            assertEquals(word, result.get(0).getWord());
            assertEquals(
                    "\n    <b>vyhledat</b> <small>[Zdeněk Brož]</small>\n",
                    result.get(0).getArticle());

            // Test prediction
            word = "looku";
            result = dict.readArticles(word);
            assertTrue(result.isEmpty());
            result = dict.readArticlesPredictive(word);
            assertEquals(1, result.size());
            assertEquals("lookup", result.get(0).getWord());
            assertEquals(
                    "\n    <b>vyhledat</b> <small>[Zdeněk Brož]</small>\n",
                    result.get(0).getArticle());
            assertEquals(StarDictDictionary.EntryType.PANGO, result.get(0).getType());
        }
    }

    @Test
    public void testReadJMDict() throws Exception {
        try (StarDictDictionary dict =
                StarDictDictionary.loadDictionary(new File("src/test/resources/dicts-syn/JMdict.ifo"))) {
            assertEquals("JMdict", dict.getDictionaryName());
            assertEquals("3.0.0", dict.getDictionaryVersion());

            String word = "くりかえし";
            List<Map.Entry<String, IndexEntry>> data = dict.data.lookUp(word);
            assertEquals(6, data.size());
            List<StarDictDictionary.Entry> result = dict.readArticles(word);
            assertEquals(6, result.size());
            assertEquals(word, result.get(0).getWord());
            assertEquals(StarDictDictionary.EntryType.HTML, result.get(0).getType());
            for (int i = 0; i < 6; i++) {
                assertTrue(result.get(i).getArticle().contains("くりかえし"));
            }
        }
    }
}
