package tokyo.northside.stardict;

import java.io.File;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

public class PerformanceTest {
    @Test
    @Timeout(60)
    public void testLoadSynonim() throws Exception {
        try (StarDictDictionary dict =
                StarDictDictionary.loadDictionary(new File("src/test/resources/dicts-syn/JMdict.ifo"))) {
            Assertions.assertEquals(370363, dict.getInformation().getSynWordCount());
        }
    }
}
