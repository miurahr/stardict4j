package tokyo.northside.stardict;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.Normalizer;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

/**
 * Dictionary driver for StarDict format.
 * <p>
 * StarDict format described on
 * <a href="https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat">stardict-3 document</a>
 * Files:
 * Every dictionary consists of these files:
 * <ol><li>somedict.ifo
 * <li>somedict.idx or somedict.idx.gz
 * <li>somedict.dict or somedict.dict.dz
 * <li>somedict.syn (optional)
 * </ol>
 *
 * @author Alex Buloichik
 * @author Hiroshi Miura
 * @author Aaron Madlon-Kay
 * @author Suguru Oho
 */
public final class StarDictLoader {

    public enum LoadFlags {
        /**
         * A flag representing the option to load diacritics.
         *
         * <p>
         * This flag can be used to specify whether diacritics should be loaded or not.
         * When this flag is set to true, diacritics will be loaded. When it is set to false,
         * diacritics will not be loaded.
         * </p>
         *
         * @see LoadFlags
         * @since 0.6.0
         */
        FLAG_LOAD_DIACRITICS
    }

    private StarDictLoader() {}

    /**
     * preload stardict information file.
     * @param ifoFile .ifo file of the dictionary to load.
     * @return StarDictInfo object.
     * @throws Exception when the header file is invalid, or i/o error occurred.
     */
    public static StarDictInfo preloadInfo(final File ifoFile) throws Exception {
        return StarDictInfo.create(readIFO(ifoFile));
    }

    /**
     * load stardict dictionary file with default options.
     * @param ifoFile .ifo file of the dictionary to load.
     * @param cacheSize size of entry cache.
     * @return StarDictDictionary object.
     * @throws Exception when dictionary file is invalid, or i/o error occurred.
     */
    public static StarDictDictionary load(final File ifoFile, final int cacheSize) throws Exception {
        return load(ifoFile, cacheSize, EnumSet.noneOf(LoadFlags.class));
    }

    /**
     * Load stardict dictionary file with custom options.
     * @param ifoFile .ifo file of the dictionary to load.
     * @param cacheSize size of entry cache.
     * @param flags load flags to tweak behavior.
     * @return StarDictDictionary object.
     * @throws Exception when dictionary file is invalid, or i/o error occurred.
     */
    public static StarDictDictionary load(final File ifoFile, final int cacheSize, EnumSet<LoadFlags> flags)
            throws Exception {
        StarDictInfo info = preloadInfo(ifoFile);
        String f = ifoFile.getPath();
        if (f.endsWith(".ifo")) {
            f = f.substring(0, f.length() - ".ifo".length());
        }
        String dictName = f;
        File idxFile = getFile(dictName, ".idx.gz", ".idx")
                .orElseThrow(() -> new FileNotFoundException("No .idx file could be found"));
        File synFile = getFile(dictName, ".syn.gz", ".syn").orElse(null);
        DictionaryData<IndexEntry> data = loadData(idxFile, synFile, info.isOftindexbits64(), info.getTypes(), flags);

        File dictFile = getFile(dictName, ".dict.dz", ".dict")
                .orElseThrow(() -> new FileNotFoundException("No .dict.dz or .dict files were found for " + dictName));

        try {
            if (dictFile.getName().endsWith(".dz")) {
                return new StarDictZipDict(info, dictFile, data, cacheSize);
            } else {
                return new StarDictFileDict(info, dictFile, data, cacheSize);
            }
        } catch (IOException ex) {
            throw new FileNotFoundException("No .dict.dz or .dict files were found for " + dictName);
        }
    }

    /**
     * Read header.
     * @param ifoFile .ifo file descriptor.
     * @return metadata in key=value map form.
     */
    private static Map<String, String> readIFO(final File ifoFile) throws Exception {
        Map<String, String> result = new TreeMap<>();
        try (BufferedReader rd = Files.newBufferedReader(ifoFile.toPath(), StandardCharsets.UTF_8)) {
            String line;
            String first = rd.readLine();
            if (!"StarDict's dict ifo file".equals(first)) {
                throw new Exception("Invalid header of .ifo file: " + first);
            }
            while ((line = rd.readLine()) != null) {
                if (line.trim().isEmpty()) {
                    continue;
                }
                int pos = line.indexOf('=');
                if (pos < 0) {
                    throw new Exception("Invalid format of .ifo file: " + line);
                }
                result.put(line.substring(0, pos), line.substring(pos + 1));
            }
        }
        return result;
    }

    private static Optional<File> getFile(final String basename, final String... suffixes) {
        return Stream.of(suffixes)
                .map(suff -> new File(basename + suff))
                .filter(File::isFile)
                .findFirst();
    }

    private static String removeDiacritics(String key) {
        // perform unicode denormalization to separate characters from marks
        String keyNfkd = Normalizer.normalize(key, Normalizer.Form.NFKD);

        // fast jump, no diacritics
        if (keyNfkd.chars().allMatch(c -> Character.getType(c) != Character.NON_SPACING_MARK)) {
            return key;
        }

        // reassemble skipping diacritic marks
        StringBuilder keyDenorm = new StringBuilder(keyNfkd.length());
        for (char c : keyNfkd.toCharArray()) {
            if (Character.getType(c) == Character.NON_SPACING_MARK) {
                continue;
            }

            keyDenorm.append(c);
        }

        return keyDenorm.toString();
    }

    private static DictionaryData<IndexEntry> loadData(
            final File idxFile,
            final File synFile,
            final boolean off64,
            final List<StarDictDictionary.EntryType> types,
            EnumSet<LoadFlags> flags)
            throws IOException {
        Map<String, Integer> synonymKeys = new HashMap<>();
        InputStream is;
        int b;

        DictionaryDataBuilder<IndexEntry> builder = new DictionaryDataBuilder<>(synonymKeys);
        is = Files.newInputStream(idxFile.toPath());
        try {
            if (idxFile.getName().endsWith(".gz")) {
                is = new GZIPInputStream(is, 8192);
            }
            try (DataInputStream idx = new DataInputStream(new BufferedInputStream(is));
                    ByteArrayOutputStream mem = new ByteArrayOutputStream()) {
                int c = 0;
                StarDictDictionary.EntryType type = null;
                while (true) {
                    if (!types.isEmpty()) {
                        c = c % types.size();
                        type = types.get(c);
                    }
                    b = idx.read();
                    if (b == -1) {
                        break;
                    }
                    if (b == 0) {
                        String key = new String(mem.toByteArray(), 0, mem.size(), StandardCharsets.UTF_8);
                        mem.reset();
                        long bodyOffset;
                        if (off64) {
                            bodyOffset = idx.readLong();
                        } else {
                            bodyOffset = idx.readInt();
                        }
                        int bodyLength = idx.readInt();

                        IndexEntry entry = new IndexEntry(key, bodyOffset, bodyLength, type);
                        if (flags.contains(LoadFlags.FLAG_LOAD_DIACRITICS)) {
                            key = removeDiacritics(key);
                        }
                        builder.add(key, entry);
                        c++;
                    } else {
                        mem.write(b);
                    }
                }
            }
        } finally {
            is.close();
        }
        if (synFile != null) {
            is = Files.newInputStream(synFile.toPath());
            try {
                if (synFile.getName().endsWith(".gz")) {
                    is = new GZIPInputStream(is, 8192);
                }
                try (DataInputStream syn = new DataInputStream(new BufferedInputStream(is));
                        ByteArrayOutputStream mem = new ByteArrayOutputStream()) {
                    while (true) {
                        b = syn.read();
                        if (b == -1) {
                            break;
                        }
                        if (b == 0) {
                            String key = new String(mem.toByteArray(), 0, mem.size(), StandardCharsets.UTF_8);
                            mem.reset();
                            int index = syn.readInt();

                            String word = key;
                            if (flags.contains(LoadFlags.FLAG_LOAD_DIACRITICS)) {
                                key = removeDiacritics(key);
                            }
                            builder.addSynonym(key, word, index);
                        } else {
                            mem.write(b);
                        }
                    }
                }
            } finally {
                is.close();
            }
        }
        return builder.build();
    }
}
