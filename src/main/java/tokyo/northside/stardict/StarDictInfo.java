/*
 * Stardict4j - access library for stardict format.
 * Copyright (C) 2022 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.stardict;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Hiroshi Miura
 */
public class StarDictInfo {
    private final String bookName;
    private final String version;
    private final int wordCount;
    private final boolean hasSynonym;
    private final int synWordCount;
    private final boolean oftindexbits64;
    private final String author;
    private final String website;
    private final String description;
    private final String date;
    private final String sametypesequence;
    private final StarDictDictionary.EntryType[] types;

    /**
     * Create StarDictInfo object from header.
     * @param header Header database in map.
     * @throws Exception when the header file is invalid.
     * @return StarDictInfo object.
     */
    public static StarDictInfo create(Map<String, String> header) throws Exception {
        String bookName = header.get("bookname");
        String version = header.get("version");
        if (!"2.4.2".equals(version) && !"3.0.0".equals(version)) {
            throw new Exception("Invalid version of dictionary: " + version);
        }
        String swc = header.getOrDefault("synwordcount", null);
        boolean hasSynonym = (swc != null);
        int synWordCount = hasSynonym ? Integer.parseUnsignedInt(swc) : 0;
        String wc = header.getOrDefault("wordcount", "0");
        int wordCount = Integer.parseUnsignedInt(wc);
        String sametypesequence = header.getOrDefault("sametypesequence", null);
        StarDictDictionary.EntryType[] types = null;
        if (sametypesequence != null) {
            types = new StarDictDictionary.EntryType[sametypesequence.length()];
            for (int i = 0; i < sametypesequence.length(); i++) {
                types[i] = StarDictDictionary.EntryType.getTypeByValue(sametypesequence.charAt(i));
                if (types[i] == null) {
                    throw new Exception("Invalid dictionary type: " + sametypesequence);
                }
            }
        }

        String author = header.getOrDefault("author", "");
        String website = header.getOrDefault("website", "");
        String description = header.getOrDefault("description", "");
        String date = header.getOrDefault("date", null);
        int idxoffsetbits = 32;
        if ("3.0.0".equals(version)) {
            String bitsString = header.get("idxoffsetbits");
            if (bitsString != null) {
                try {
                    idxoffsetbits = Integer.parseInt(bitsString);
                } catch (NumberFormatException ignored) {
                }
                if (idxoffsetbits != 32 && idxoffsetbits != 64) {
                    throw new Exception("StarDict dictionaries other than idxoffsetbits=64 or 32 are not supported.");
                }
            }
        }
        return new StarDictInfo(
                bookName,
                version,
                hasSynonym,
                synWordCount,
                wordCount,
                sametypesequence,
                author,
                website,
                description,
                date,
                (idxoffsetbits == 64),
                types);
    }

    /**
     * Constructor.
     * @param header Header database in map.
     * @throws Exception when wrong header values.
     */
    @Deprecated(forRemoval = true)
    public StarDictInfo(Map<String, String> header) throws Exception {
        bookName = header.get("bookname");
        version = header.get("version");
        if (!"2.4.2".equals(version) && !"3.0.0".equals(version)) {
            throw new Exception("Invalid version of dictionary: " + version);
        }
        String swc = header.getOrDefault("synwordcount", null);
        hasSynonym = (swc != null);
        synWordCount = hasSynonym ? Integer.parseUnsignedInt(swc) : 0;
        String wc = header.getOrDefault("wordcount", "0");
        wordCount = Integer.parseUnsignedInt(wc);
        sametypesequence = header.getOrDefault("sametypesequence", null);
        StarDictDictionary.EntryType[] types = null;
        if (sametypesequence != null) {
            types = new StarDictDictionary.EntryType[sametypesequence.length()];
            for (int i = 0; i < sametypesequence.length(); i++) {
                types[i] = StarDictDictionary.EntryType.getTypeByValue(sametypesequence.charAt(i));
                if (types[i] == null) {
                    throw new Exception("Invalid dictionary type: " + sametypesequence);
                }
            }
        }

        author = header.getOrDefault("author", "");
        website = header.getOrDefault("website", "");
        description = header.getOrDefault("description", "");
        date = header.getOrDefault("date", null);
        int idxoffsetbits = 32;
        if ("3.0.0".equals(version)) {
            String bitsString = header.get("idxoffsetbits");
            if (bitsString != null) {
                try {
                    idxoffsetbits = Integer.parseInt(bitsString);
                } catch (NumberFormatException ignored) {
                }
                if (idxoffsetbits != 32 && idxoffsetbits != 64) {
                    throw new Exception("StarDict dictionaries other than idxoffsetbits=64 or 32 are not supported.");
                }
            }
        }
        this.oftindexbits64 = idxoffsetbits == 64;
        this.types = types;
    }

    private StarDictInfo(
            String bookName,
            String version,
            boolean hasSynonym,
            int synWordCount,
            int wordCount,
            String sametypesequence,
            String author,
            String website,
            String description,
            String date,
            boolean idxoffsetbits64,
            StarDictDictionary.EntryType[] types) {
        this.bookName = bookName;
        this.version = version;
        this.hasSynonym = hasSynonym;
        this.synWordCount = synWordCount;
        this.wordCount = wordCount;
        this.sametypesequence = sametypesequence;
        this.author = author;
        this.website = website;
        this.description = description;
        this.date = date;
        this.oftindexbits64 = idxoffsetbits64;
        this.types = types;
    }

    /**
     * Return book name.
     * @return book name.
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * Return version string.
     * @return "2.4.2" or "3.0.0".
     */
    public String getVersion() {
        return version;
    }

    /**
     * Return word count.
     * @return word count.
     */
    public int getWordCount() {
        return wordCount;
    }

    /**
     * Does dictionary has synonym?
     * @return true when .syn file exist, otherwise false.
     */
    @SuppressWarnings("unused")
    public boolean isHasSynonym() {
        return hasSynonym;
    }

    /**
     * Return synonym word count.
     * @return synonym word count when specified, otherwise 0.
     */
    @SuppressWarnings("unused")
    public int getSynWordCount() {
        return synWordCount;
    }

    /**
     * Is dictionary index is 64bit?
     * @return true when index field is 64 bit, otherwise false.
     */
    @SuppressWarnings("unused")
    public boolean isOftindexbits64() {
        return oftindexbits64;
    }

    /**
     * Return author string.
     * @return author when specified, otherwise empty string.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Return website url string.
     * @return website url when exist, otherwise empty string.
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Return description.
     * @return description when exist, otherwise empty string "".
     */
    public String getDescription() {
        return description;
    }

    /**
     * Return date field string.
     * @return date field string.
     */
    public String getDate() {
        return date;
    }

    /**
     * Return sametypesequence field value string.
     * @return string
     */
    public String getSametypesequence() {
        return sametypesequence;
    }

    public List<StarDictDictionary.EntryType> getTypes() {
        if (types == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(Arrays.asList(types));
    }
}
