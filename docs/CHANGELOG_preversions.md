# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased]

## [v1.0.0]

### Changed

- Bump dictzip@1.0.0
- Chore: update gradle plugins
- Chore: enable gradle develocity plugin

## [v0.6.3]

### Fixed

- Fix a loading dictionary with synonyms (#47)

## [v0.6.2]

### Fixed

- Fix a performance problem when large synonyms exist in a dictionary.(#46)

## [v0.6.1]

### Fixed

- Add missing javadoc documentation for changed methods
- Update StarDictPropietaryTest assertions to reflect a change in v0.6.0

### Changed

- CI: fail on warning for javadoc documentation issues

## [v0.6.0]

### Fixed

- Make key returned by lookUpPredictive to be an original dictionary data key.

### Added

- Add unicode de-normalization and diacritic removal load option (#41)
- Support Pango markup style

### Changed

- Change a package path to 'tokyo.northside.stardict'
- Add module-info.java for JPMS
- chore: Use version catalog for dependencies
- chore: allow build from git export archive

## [v0.5.1]

### Changed

- Bump minimum Java requirement to 11
- Bump gradle@8.3
- Gradle use version catalog

## [v0.5.0]

### Changed

- Drop a Caffeine cache library and use simple cache(#39)
- Bump gradle@7.5.1
- gradle: use java toolchain spec for java version
- Lint: Update checkstyle rules

## [v0.4.1]

### Fixed

- Fix downstream build error with
  "Cannot choose between the following variants of ...: - runtimeElements, shadowRuntimeElements"

### Added

- Support data that has no sametypesequence header data

### Changed

- Bump gradle@7.4.2

## [v0.4.0]

- Migrate forge site to codeberg.org
- Bump gradle@7.3.3
- Gradle: Improve signatory configuration
- Gradle: bundle version.properties in source jar

## [v0.3.2]

- Bump dictzip@0.12.2

## [v0.3.1]

- Fix Null-Pointer exception when there are multiple articles in single
  entry with certain condition.(#11)
- Cache expiry from last access not write(#13)
- Bump versions
  - spotless@6.3.0
  - spotbugs@5.0.6
  - actions/setup-java@v3
  - dictzip@0.12.1
- Add a test case with proprietary data
  - commit without the data

## [v0.3.0]

- Introduce `StarDictDictionary#loadDictionary` builder utility method.
- Hide `StarDictLoader#load` method.
- Don't search lowercase automatically.
- Add cache mechanism for articles.

## [v0.2.0]

- Change class names

## [v0.1.1]

- Fix javadoc

## v0.1.0

[Unreleased]: https://codeberg.org/miurahr/stardict4j/compare/v1.0.0...HEAD
[v1.0.0]: https://codeberg.org/miurahr/stardict4j/compare/v0.6.3...v1.0.0
[v0.6.3]: https://codeberg.org/miurahr/stardict4j/compare/v0.6.2...v0.6.3
[v0.6.2]: https://codeberg.org/miurahr/stardict4j/compare/v0.6.1...v0.6.2
[v0.6.1]: https://codeberg.org/miurahr/stardict4j/compare/v0.6.0...v0.6.1
[v0.6.0]: https://codeberg.org/miurahr/stardict4j/compare/v0.5.1...v0.6.0
[v0.5.1]: https://codeberg.org/miurahr/stardict4j/compare/v0.5.0...v0.5.1
[v0.5.0]: https://codeberg.org/miurahr/stardict4j/compare/v0.4.1...v0.5.0
[v0.4.1]: https://codeberg.org/miurahr/stardict4j/compare/v0.4.0...v0.4.1
[v0.4.0]: https://codeberg.org/miurahr/stardict4j/compare/v0.3.2...v0.4.0
[v0.3.2]: https://codeberg.org/miurahr/stardict4j/compare/v0.3.1...v0.3.2
[v0.3.1]: https://codeberg.org/miurahr/stardict4j/compare/v0.3.0...v0.3.1
[v0.3.0]: https://codeberg.org/miurahr/stardict4j/compare/v0.2.0...v0.3.0
[v0.2.0]: https://codeberg.org/miurahr/stardict4j/compare/v0.1.1...v0.2.0
[v0.1.1]: https://codeberg.org/miurahr/stardict4j/compare/v0.1.0...v0.1.1
